import com.zuitt.wdc0043_s4s5_a1.*;

public class Main {
    public static void main(String[] args) {

        Contact john_doe = new Contact("John Doe","2222222","3333333","Makati City", "Taguig City");
        Contact jane_doe = new Contact("Jane Doe","4444444","5555555","Mandaluyong City", "Quezon City");
//        john_doe.printContactInfo();

        Phonebook phonebook = new Phonebook();
        phonebook.getContacts().add(john_doe);
        phonebook.getContacts().add(jane_doe);

        if(phonebook.getContacts().isEmpty()){
            System.out.println("The phonebook is empty");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                System.out.println(contact.getContactNumber1());
                System.out.println(contact.getContactNumber2());
                System.out.println("-------------------------------------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println("my home in " + contact.getAddress1());
                System.out.println("my office in " + contact.getAddress2());
                System.out.println("=================================================");
            }
        }





    }


}