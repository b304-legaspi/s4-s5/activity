package com.zuitt.wdc0043_s4s5_a1;

import java.util.ArrayList;


public class Phonebook {

    private ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook(){}

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }


}
